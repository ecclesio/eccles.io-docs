# Staff
_People are powered by the [Church Theme Content Plugin](https://churchthemes.com/guides/user/content/people/), so much of this documentation is from their site but placed here for ease of access and may be modified specifically for use on [Eccles.io](https://eccles.io)._

You can add people with bios, pictures and contact information. They can optionally be placed into groups such as staff, deacons, etc.

## People Page

First create a page that will list all people.

1. Go to **Pages** > **Add New**.
2. Enter a title and any content that you want to appear at the top of the page.
3. Under _Page Attributes_, set _Template_ to "People".
4. Optionally set a _Featured image _(see [Section Banners][1] for related information)_ ._
5. Click **Publish_._**

## Groups

Optionally organize people into groups such as staff, deacons, etc. You can then use a widget in the _People Sidebar_ (if available with your theme) to list the groups or link to them from your menu.

* Go to **People**  > **Groups**.
* Complete the form to add a group.
* View, edit and delete them on the right.

## Managing People

### Adding a Person

1. Go to **People** > **Add New**.
2. Enter the person's name.
3. Write a bio for the person.
4. Fill in their details under _Person Details._
5. Enter a short description for the profile in _Excerpt_.
6. Optionally select or add the Group that this person belongs to.
7. Optionally click **Set featured image **to upload an portrait of the person.
8. Click **Publish**.

### Editing and Deleting People

1. Go to **People** > **All People**.
2. Hover over a person to see the **Edit** and **Trash** options.

### Ordering People

You can easily drag and drop people in the order you would like them to appear on your website.