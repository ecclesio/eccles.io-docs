# Sermons

_Sermons are powered by the [Church Theme Content Plugin](https://churchthemes.com/guides/user/content/sermons/), so much of this documentation is from their site but placed here for ease of access and may be modified specifically for use on [Eccles.io](https://eccles.io)._

## Sermons Page

By default, there is a sermons index page at _yourdomain.com/sermons_ but if you need to create an additional sermons page, follow these steps.

1. Go to Pages > Add New.
2. Enter a title (e.g. “Sermon Archive”) and any content that you want to appear above the sermons.
3. Under Page Attributes, set Template to “Sermons”.
- Optionally set a Featured image (see Section Banners for related information) 
4. Click Publish.

## Topics, Books, Series and Speakers

Sermons can be organized by topic, book, series and speaker (these are taxonomies). You can add these while adding a new sermon but there is also an area dedicated to managing them.

- Go to *Sermons* then *Topics, Books, Series* or *Speakers*.
- Complete the form to add one.
- View, edit and delete them on the right.

## Publishing Sermons

You can publish sermons with text, audio, video and PDF content. Audio and video can be uploaded via WordPress or used from another site such as YouTube.

### Adding a Sermon

1. Go to **Sermons** > **Add New**.
2. Enter the sermon's title
3. Optionally enter written sermon content (ie. full transcript or short version)
4. Provide any combination of the following **Sermon Media**. 
    * **Full Text** – Check this if written sermon content was entered
    * **Video** – See instructions under field (upload, URL or embed code)
    * **Audio – **See instructions under field (upload, URL or embed code)
    * **PDF** – See instructions under field (upload or URL)
5. Enter a two or three sentence **Excerpt** introducing your sermon (shown on sermon lists).
6. Select or add the topics, books, series, speakers and tags for this sermon (all optional).
7. Optionally click **Set featured image **to upload an image representing the sermon.
8. Click **Edit** by _Publish immediately_ to enter the date of the sermon.
9. Click **Publish**.

### Editing and Deleting Sermons

1. Go to **Sermons** > **All Sermons**.
2. Hover over a sermon to see the **Edit** and **Trash** options.