# Events

Events are powered by the [Church Theme Content Plugin](https://churchthemes.com/guides/user/content/events/), so much of this documentation is from their site but placed here for ease of access and may be modified specifically for use on [Eccles.io](https://eccles.io)._

You can publish single or multiple-day events. They can recur weekly, monthly or yearly (with optional end date). Maps and directions can also be included. Events can be shown in a monthly calendar or in list format.

## Event Categories

Optionally organize events into categories such as children, classes, etc. You can then use a widget in the _Events Sidebar_ to list the categories or link to them from your menu. Some of our themes do not have a sidebar. Instead, they automatically list event categories in the header.

* Go to **Events**  > **Categories**.
* Complete the form to add a category.
* View, edit and delete them on the right.

## Managing Events

### Adding an Event

1. Go to **Events** > **Add New**.
2. Enter a title for the event.
3. Use the content editor to write information about the event.
4. Fill in the details under _Date & Time_ (_Start Date_ is required).
5. Fill in the optional details under _Location_ (venue, address, map).
6. Optionally enter details for _Registration_(see Event Registration below).
7. Write a short _Excerpt_ summarizing the event.
8. Optionally select or add the Category that this event belongs to.
9. Optionally click **Set featured image** to set upload an image representing the event.
10. Click **Publish**.

### Editing and Deleting Events

1. Go to **Events** > **All Events**.
2. Hover over an event to see the **Edit** and **Trash** options.

# Recurring Events

Events can be set to recur weekly, monthly or yearly on the same day. When an event ends, its dates are automatically moved forward. You can use this feature by choosing an option in the _Recurrence_field. You may optionally provide a date for automatic recurrence to end after.

## Event Registration

There are several solutions you can use for event registration.

1. A basic solution is to use the pre-installed form manager to embed a registration form into your event's content. The form will email you each registration (form submission) and store each entry in the admin area _(under Forms>Entries)_
2. If you are using a church management system (ChMS), it may have an event registration feature that you can link out to. You can use the _Registration URL_ field for this and it will cause a Registration button to show.  
3. If you need something more sophisticated (e.g. selling tickets), consider linking out to a service like EventBrite (free for free events) or Bizzabo. TypeForm is another possibility. You can use the _Registration URL_ field for this also.
> If you are looking for a great, free Church Management system, we recommend [Rock RMS](https://www.rockrms.com/)